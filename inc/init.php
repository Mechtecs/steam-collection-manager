<?php

require_once('config.php');
require_once('mysql.php');
require_once('app.php');

global $mysql;
$mysql = new mysql();

global $app;
$app = new app();
