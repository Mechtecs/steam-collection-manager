<?php

require_once "steam.php";

class mysql {

    function __construct() {
        $this->mysql = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
        $this->steam = new steam(WEB_API_KEY);
        $this->mysql->query("CREATE TABLE IF NOT EXISTS `collection_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `remote_id` int(11) DEFAULT NULL,
  `state` enum('PRESENT','ABSENT','CONFIRMED') NOT NULL,
  `collection_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `remote_id` (`remote_id`)
) ENGINE=InnoDB AUTO_INCREMENT=339 DEFAULT CHARSET=utf8;");
        $this->mysql->query("CREATE TABLE IF NOT EXISTS `collections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `remote_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `remote_id` (`remote_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;");
    }

    /**
     * @return array
     */
    public function get_collection($id) {
        return $this->mysql->query("SELECT * FROM collections WHERE id = '$id' OR remote_id = '$id' LIMIT 1")->fetch_assoc();
    }

    /**
     * @param $id
     * @return bool|mysqli_result
     */
    public function get_collection_items($id) {
        return $this->mysql->query("SELECT * FROM collection_items WHERE collection_id = '" . $this->get_collection($id)["id"] . "'");
    }

    /**
     * @return bool|mysqli_result
     */
    function get_all_collections($additional_info = false) {
        if($additional_info) {
            return $this->mysql->query('SELECT id, remote_id, created_at, updated_at, (SELECT count(*) FROM collection_items WHERE collection_id = collections.id AND state = "present") AS maps, (SELECT count(*) FROM collection_items WHERE collection_id = collections.id AND state = "absent") AS maps_absent FROM collections', MYSQLI_ASSOC);
        } else {
            return $this->mysql->query('SELECT * FROM collections', MYSQLI_ASSOC);
        }
    }

    /**
     * @param $id
     */
    function update_collection($id) {
        $collection = $this->mysql->query("SELECT * FROM collections WHERE remote_id = '$id' LIMIT 1")->fetch_assoc();
        $this->mysql->query("UPDATE collection_items SET state = 'absent' WHERE collection_id = '" . $collection["id"] . "' AND state != 'confirmed'");
        $response = $this->steam->get_collection_details($id);
        if(isset($response["response"]) && $response["response"]["result"] === 1 && $response["response"]["collectiondetails"][0]["result"] === 1) {
            foreach($response["response"]["collectiondetails"][0]["children"] as $map) {
                $this->mysql->query("INSERT INTO collection_items (remote_id, state, collection_id) VALUES ('" . $map["publishedfileid"] . "', 'PRESENT', " . $collection["id"] . ") ON DUPLICATE KEY UPDATE state = 'present'") OR die($this->mysql->error);
            }
        } else {
            $this->delete_collection($id);
        }
    }

    /**
     * @param $id
     * @return bool|mysqli_result
     */
    public function delete_collection_item($id) {
        return $this->mysql->query("DELETE FROM collection_items WHERE id = '$id'");
    }

    /**
     * @param $remote_id int
     * @return bool
     */
    function create_collection($remote_id) {
        if($this->mysql->query("INSERT INTO collections (remote_id) VALUES ('$remote_id') ON DUPLICATE KEY UPDATE updated_at = CURRENT_TIMESTAMP()")) {
            $this->update_collection($remote_id);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return bool|mysqli_result
     */
    function delete_collection($id) {
        return $this->mysql->query("DELETE FROM collections WHERE id = $id OR remote_id = $id");
    }

    public function update_all_collections() {
        $collections = $this->mysql->query("SELECT remote_id FROM collections");
        while($collection = $collections->fetch_assoc()) {
            $this->update_collection($collection["remote_id"]);
        }
    }
}
