<?php

class app {

    public function handle() {
        global $mysql;

        if((isset($_POST["action"]) && $action = $_POST["action"]) || (isset($_GET["action"]) && $action = $_GET["action"] )) {
            if($action === "add_collection") {
                $mysql->create_collection($_POST["remote_id"]);
            } elseif ($action === "delete_collection") {
                $mysql->delete_collection($_GET["id"]);
            } elseif ($action === "update") {
                $mysql->update_all_collections();
            } elseif ($action === "view" && isset($_GET["delete"])) {
                $mysql->delete_collection_item($_GET["delete"]);
            }
        }
    }

}
