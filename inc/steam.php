<?php

class steam {

    function __construct($apikey) {
        $this->apikey = $apikey;
    }

    public function get_collection_details($collection_id) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://api.steampowered.com/ISteamRemoteStorage/GetCollectionDetails/v1/");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query(array('key' => $this->apikey, 'collectioncount' => 1, 'publishedfileids[0]' => $collection_id)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = json_decode(curl_exec($ch), true);

        curl_close ($ch);

        return $response;
    }
}
