<?php

require_once('inc/init.php');

global $app;
$app->handle();

global $mysql;
$collections = $mysql->get_all_collections(true);
?>

<html>
    <head>
        <title>Collection Manager</title>
    </head>
    <body>
        <h1>Collection Manager</h1>
        <h2>Collections:</h2>
        <table>
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Map Count</td>
                    <td>Missing Maps</td>
                    <td>Actions</td>
                </tr>
            </thead>
            <tbody>
                <?php while($collection = $collections->fetch_assoc()): ?>
                <tr>
                    <td><a href="http://steamcommunity.com/sharedfiles/filedetails/?id=<?= $collection["remote_id"] ?>"><?= $collection["remote_id"] ?></a></td>
                    <td><?= $collection["maps"] ?></td>
                    <td><?= $collection["maps_absent"] ?></td>
                    <td><a href="index.php?action=view&id=<?= $collection["id"] ?>">View</a>   <a href="index.php?action=delete_collection&id=<?= $collection["id"] ?>">Delete</a></td>
                </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
        <form action="index.php" method="post">
            <input type="number" id="remote_id" name="remote_id">
            <input type="submit" value="Add collection">
            <input type="hidden" name="action" value="add_collection">
        </form>
        <form action="index.php" method="post">
            <input type="hidden" name="action" value="update">
            <input type="submit" value="Update All!">
        </form>
        <?php if(isset($_GET["action"]) && $_GET["action"] === "view"):
            $current_collection = $mysql->get_collection($_GET["id"]); ?>
            <h2>Collection <a href="http://steamcommunity.com/sharedfiles/filedetails/?id=<?= $current_collection["remote_id"] ?>"><?= $current_collection["remote_id"] ?></a></h2>
            <table>
                <thead>
                    <tr>
                        <td>#ID</td>
                        <td>State</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $children = $mysql->get_collection_items($current_collection["id"]);
                        while($child = $children->fetch_assoc()):
                    ?>
                        <tr>
                            <td><a href="http://steamcommunity.com/sharedfiles/filedetails/?id=<?= $child["remote_id"] ?>"><?= $child["remote_id"] ?></a></td>
                            <td><?= $child["state"] ?></td>
                            <td><a href="index.php?action=view&id=<?= $_GET["id"] ?>&delete=<?= $child["id"] ?>">Delete</a></td>
                        </tr>
                    <?php
                        endwhile;
                    ?>
                </tbody>
            </table>
        <?php endif; ?>
    </body>
</html>
